import json
import logging
import time

import psycopg2
import requests

import request_builder

# class Report(NamedTuple):
#     Representation of Report model for database
#     Date: str|nullable
#     CampaignName: str|nullable
#     CampaignId: str|nullable
#     CampaignType: str|nullable
#     AdGroupName: str|nullable
#     AdGroupId: str|nullable
#     AdFormat: str|nullable
#     AdId: str|nullable
#     CriterionId: str|nullable
#     Criterion: str|nullable
#     CriterionType: str|nullable
#     AdNetworkType: str|nullable
#     Device: str|nullable
#     MobilePlatform: str|nullable
#     LocationOfPresenceId: str|nullable
#     RlAdjustmentId: str|nullable
#     Placement: str|nullable
#     Slot: str|nullable
#     Bounces: str|nullable
#     BounceRate: str|nullable
#     Impressions: str|nullable
#     Clicks: str|nullable
#     Ctr: str|nullable
#     AvgCpc: str|nullable
#     Cost: str|nullable
#     Revenue: str|nullable
#     GoalsRo: str|nullable

reports = {}
length = 0


def main() -> None:
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    try:
        with open("config.json", "r") as config:
            data = json.load(config)
    except Exception as e:
        print(f"{e}")
        exit(1)

    if data is None:
        raise Exception("data value is none, recreate the file")

    print(f"{data}")

    _assert_data(data)
    _pr_for_request(data)

    for field_property in data["params"]["FieldNames"]:
        reports[field_property] = []
    print("answer has been gotten")

    # custom structure for db
    _read_csv_as_structure()
    _write_data_to_db(data)
    exit(0)


def _pr_for_request(data: dict) -> None:
    rb = request_builder.RequestBuilder()
    rb.set_headers(data["token"], data["clientLogin"])
    rb.set_body(data["params"])
    r = rb.get_request()
    _request_loop(r)


def _request_loop(request: dict):
    request_url = "https://api.direct.yandex.com/json/v5/reports"
    request_headers = request["headers"]
    request_body = json.dumps(request["body"], indent=2)
    result_csv = ";".join(request["body"]["params"]["FieldNames"]) + "\n"
    while True:
        try:
            req = requests.post(request_url, request_body, headers=request_headers)
            req.encoding = "utf-8"
            retry = int(req.headers.get("retryIn", 60))
            request_id = req.headers.get("RequestId", False)
            print(f"Status code: {req.status_code}")
            print(f"RequestId: {request_id}")
            print(f"retryIn: {retry}")
            if req.status_code == 200:
                print("RequestId: {}".format(req.headers.get("RequestId", False)))
                if req.text != "":
                    temp_result = req.text.split('\t')
                    result_csv += ";".join(temp_result) + "\n"
                break
            else:
                print(f"JSON request body: {request_body}")
                print(f"JSON answer: \n{req.json()}")
                with open("fail.txt", "w") as file:
                    file.write(str(req.json()))
                if req.status_code == 201 or req.status_code == 202:
                    time.sleep(retry)
                else:
                    break
        except ConnectionError:
            print("Произошла ошибка соединения с сервером API")
            break
        except:
            print("Произошла непредвиденная ошибка")
            break
    with open("result.csv", "w+") as file:
        file.write(result_csv)


def _read_csv_as_structure():
    with open("result.csv", "r") as csv:
        lines = csv.readlines()
    if lines is not []:
        line_values = []
        length = len(lines) - 1
        for i in range(0, len(lines)):
            line_values.append([lines[0].split(";")])
        for i in range(1, len(line_values)):
            for j in range(0, len(line_values[i])):
                reports[line_values[0][j]] = line_values[i][j]
    else:
        raise Exception("csv file is empty or does not exist")


def _write_data_to_db(data):
    connection = psycopg2.connect(dbname=data["dbConnection"]["dbname"], user=data["dbConnection"]["user"],
                                  host=data["dbConnection"]["host"], port=data["dbConnection"]["port"])
    try:
        cursor = connection.cursor()
        create_table_query = """
        create table {} (id int constraint {}_pk primary key,
        """.format(data["clientLogin"], data["clientLogin"])
        create_table_query += " text,\n".join(reports.keys()) + " text);"
        cursor.execute(create_table_query)
        connection.commit()
        connection.close()
        cursor.close()
    except:
        pass

    try:
        cursor = connection.cursor()
        query = 'insert into {0}({1}) values ({2})'
        for i in range(length):
            query = query.format(data["clientLogin"], ','.join(reports.keys()), ','.join('?' * len(reports.keys())))
            values = [item[i] for item in reports.values()]
            cursor.execute(query, values)
        connection.commit()
        connection.close()
        cursor.close()
    except:
        pass


def _assert_data(data: dict) -> None:
    # move to tests

    # request params
    assert "params" in data
    assert "token" in data
    assert "clientLogin" in data
    assert "ReportName" in data["params"]
    assert "DateRangeType" in data["params"]
    assert "Format" in data["params"]
    assert "IncludeVAT" in data["params"]


if __name__ == "__main__":
    main()
