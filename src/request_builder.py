class RequestBuilder(object):

    def __init__(self):
        self._reset()

    def _reset(self):
        self.headers = {}
        self.body = {}

    def set_headers(self, token: str, client_login: str, return_money_in_micros: bool = False,
                    skip_report_header: bool = False,
                    skip_report_sum: bool = False) -> None:
        self.headers = {
            "Authorization": "Bearer " + token,
            "Client-Login": client_login,
            "Accept-Language": "ru",
            "processingMode": "auto",
            "returnMoneyInMicros": "true" if return_money_in_micros else "false",
            "skipReportHeader": "true" if skip_report_header else "false",
            "skipReportSummary": "true" if skip_report_sum else "false"
        }

    def set_body(self, params: dict):
        self.body = {
            "params": params
        }

    def get_request(self) -> dict:
        if self.headers is {}:
            raise Exception("Headers is not set")
        if self.body is {}:
            raise Exception("Body is not set")
        return {
            "headers": self.headers,
            "body": self.body
        }
